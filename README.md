# Template for creating django project template

## Steps to deploy

1. Create a new project repo
   - Use github/gitlab
2. Checkout the repository you just created (do it in PyCharm or another IDE, get with the times Boomer!)
3. Create a virtualenv, and pip install the requirements file
4. Create a django project using the [best Django template around](https://gitlab.com/rpmrpm/django_project_template)
   >```python3 manage.py startproject --template https://gitlab.com/rpmrpm/django_project_template __PROJECT_NAME__ .```
5. Add apps to requirements.txt, INSTALLED_APPS (settings.py), urls.py etc..
5. Start the docker containers
   >```docker-compose up ```
6. Happy Development!

## Environment Variables

```shell
DJANGO_SITE_TITLE, "Admin Site"

DJANGO_SECRET_KEY, "{{ secret_key }}"
DJANGO_DATABASE_URL, "sqlite://"

DJANGO_LANGUAGE_CODE, "en-us"
DJANGO_TIME_ZONE, "UTC"

DJANGO_DATABASE_CONN_MAX_AGE, None
DJANGO_DATABASE_SSL_REQUIRE, False

SENTRY_DSN, None
SENTRY_ENVIRONMENT, None

DJANGO_EMAIL_HOST, "SMTP_RELAY"
DJANGO_EMAIL_PORT 25

DJANGO_REDIS_URL, "redis://redis:6379"

DJANGO_DEBUG "false"
DJANGO_MEDIA_URL, "/media/"
DJANGO_STATIC_URL, "/static/"
DJANGO_ALLOW_ASYNC_UNSAFE "true"
DJANGO_SETTINGS_MODULE, "{{ project_name }}.settings"
```